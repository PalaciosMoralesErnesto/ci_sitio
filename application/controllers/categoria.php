<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categoria extends CI_Controller
{
  private $datos;
  
  public function __construct()
  {
    parent::__construct();
	
	  $this->datos = array(
	  'categoria'   => 'Inicio',
	  
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',
	  );
  }
  
  public function index()
  {
	  
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$this->datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }

  public function ciencias_humanas()
  {
	  $this->datos = array(
	  'categoria'   => 'Ciencias Humanas',
	  'humanas'     => '#000b8b',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',
	  );
	
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$this->datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }

  public function ciencias_tecnicas()
  {
	  $datos = array(
	  'categoria'   => 'Ciencias Tecnicas',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#000b8b',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',	  
	  );
	  
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }
  
  public function derecho()
  {
	  $datos = array(
	  'categoria'   => 'Ciencias Tecnicas',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#000b8b',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',	  
	  );
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }
  
  public function economia()
  {
	$datos = array(
	  'categoria'   => 'Economia',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#000b8b',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',	  
	  );
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }
  
  public function informatica()
  {
	  $datos = array(
	  'categoria'   => 'Informatica',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#000b8b',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',	  
	  );
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }
  
  public function libros_texto()
  {
	  $datos = array(
	  'categoria'   => 'Libros de Texto',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#000b8b',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',	  
	  );
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }

  public function literatura()
  {
	  $datos = array(
	  'categoria'   => 'Literatura',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#000b8b',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',	  
	  );
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }
  
  public function oposiciones()
  {
	   $datos = array(
	  'categoria'   => 'Oposiciones',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#000b8b',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#e7e8f4',	  
	  );
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }
  
  public function tiempo_libre()
  {
	   $datos = array(
	  'categoria'   => 'Tiempo Libre',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#000b8b',
	  'novedades'   => '#e7e8f4',	  
	  );
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }
  
  public function novedades()
  {
	  $datos = array(
	  'categoria'   => 'Novedades',
	  'humanas'     => '#e7e8f4',
	  'tecnicas'    => '#e7e8f4',
	  'derecho'     => '#e7e8f4',
	  'economia'    => '#e7e8f4',
	  'informatica' => '#e7e8f4',
	  'texto'       => '#e7e8f4',
	  'literatura'  => '#e7e8f4',
	  'oposiciones' => '#e7e8f4',
	  'libre'       => '#e7e8f4',
	  'novedades'   => '#000b8b',	  
	  );
	  $this->load->view("libro/vista_cabecera.php");
	  $this->load->view("libro/vista_menu.php",$datos);
	  $this->load->view("libro/vista_pie_pagina.php");
  }
}
