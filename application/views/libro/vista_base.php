<html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://ogp.me/ns#" xmlns="http://www.w3.org/1999/xhtml">
<head><title>   Libros Urgentes. Sólo libros  </title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"></meta>
<meta content="Agapea. La librería con más libros españoles en stock del mundo" name="description"></meta>
<meta content="Agapea. La librería con más libros españoles en stock del mundo" name="DC.Description"></meta>
<meta content="index,follow" name="robots"></meta>
<meta content="131427463601521" property="fb:app_id"></meta>
<link href="/favicon.ico" rel="shortcut icon"></link>
<link rel=stylesheet type="text/css" href="/sitio/recursos/css/estilosV25z.css"/>
<script type="text/javascript"  src="/sitio/recursos/js/js43z.js">
</script>
</head>
	
<body>

	
<div id=cb2>
	<!-----------------------------  inicio  cabecera ----------------------------------------------> 
	<?php
		$this->load->view("libro/vista_cabecera.php");
	?>  
    
	<!-----------------------------  inicio menu      ---------------------------------------------> 
	<?php
		$this->load->view("libro/vista_menu.php");
	?>
	<!-----------------------------   fin menu        ----------------------------------------------> 
</div> 
    
	<hr class="hd"> </hr>
	<div class="cl"> </div>

<!-----------------------------Inicio pie de pagina  ----------------------------------------> 
	<?php
		$this->load->view("libro/vista_pie_pagina.php");
	?>

</body>
   
</html>
